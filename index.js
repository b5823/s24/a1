// ----------> MY SOLUTION <----------
// NUMBER 1
const actNum = 2 ** 3
console.log(`The cube of 2 is ${actNum}`);

// NUMBER 2
const address = ["258", "Washington Ave NW", "California", "90011"];

const [address1, address2, address3, address4] = address
console.log(`I live at ${address1} ${address2}, ${address3} ${address4}`)

// NUMBER 3
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
};

const {name, species, weight, measurement} = animal
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`)

// NUMBER 4
const characters = ['Ironman', 'Black Panther', 'Dr. Strange', 'Shang-Chi', 'Falcon']
characters.forEach(character => console.log(`${character}`));

// NUMBER 5
class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let dog1 = new Dog("Puchi", 1, "Miniature Dachshund");
console.log(dog1);

let dog2 = new Dog("Chiso", 2, "Golden Retriever");
console.log(dog2);
